// スプレッドシートのURL
const SHEET_URL = "<スプレッドシートのURL>";
// DiscordのWebhook URL
const DISCORD_URL = "<DiscordのWebhook URL>";


// 副団長アビのBot文言役職名
const STR_SUB_LEADER  = "副団長";
// 攻撃隊長・防御隊長アビのBot通知文言役職名 
const STR_ATK_DEF_ACE = "攻撃隊長・防御隊長";


// Bot通知文言の頭につける文字（例えば、@団員など）。なければ空文字[""]。
const STR_PREFIX_BOT_MSG = "";
/* Bot通知文言の最後につける文字（例えば、@団員など）。なければ空文字[""]。
   「～アビ～分前」という文言はあらかじめついています。 */
const STR_SUFFIX_BOT_MSG = "";

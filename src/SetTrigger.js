/**
 * 毎日5時ごろに確認し、古戦場中であればトリガーを作成する
 */
function chackDailyAndMakeTrigger() {
  // 事前にトリガーがあれば全て削除
  removeAllTrigger();

  // スプレッドシートデータ取得
  const sheetData = getSheetData();

  // 古戦場開催中かどうか
  const nowEvent = isNowEventTime(sheetData);
  if (nowEvent == false) {
    Logger.log("Not Unite and Fight");
    return;
  }

  // 団アビ時間を取得
  const guildAbilityDate = getGuildAbilityDate(sheetData);

  // トリガーをセット
  setTrigger(guildAbilityDate);
}

/**
 * 古戦場開催かどうか
 * @param {Object[][]} sheetData - スプレッドシートデータ
 * @return {boolean} - true or false
 */
const isNowEventTime = (sheetData) => {
  // 古戦場開始、終了時刻
  const eventStart = sheetData[0][1];
  const eventEnd   = sheetData[1][1];
  eventEnd.setHours(23,59,59);
  
  // 現在時刻が古戦場中かどうか
  const nowTime = new Date();
  if (eventStart.getTime() <= nowTime.getTime() && nowTime.getTime() <= eventEnd.getTime()) {
      return true;
  }
  
  return false;
}

/**
 * 古戦場のデータを作成、開催中でなければfalse
 * @param {Object[][]} sheetData - スプレッドシートデータ
 * @return {Object} - 団アビ時間のオブジェクト
 */
const getGuildAbilityDate = (sheetData) => {
  // 団アビ発動時間
  let abilityDate = new Object();
  abilityDate.subLeader        = new Array(2); // 副団長アビの時間
  abilityDate.attackAndDefence = new Array(2); // 攻防隊長アビの時間

  // 列番号
  const ROW_SUB_LEADER  = 4;
  const ROW_ATK_DEF_ACE = 5;
  const ROW_NOTICE_BEFORE_TIME = 7;

  // 副団長アビの時間を取得
  for (let i = 0; i < 2; i++) {
    abilityDate.subLeader[i] = sheetData[ROW_SUB_LEADER][i + 1];
  }

  // 攻撃・防御隊長アビの時間を取得
  for (let i = 0; i < 2; i++) {
    abilityDate.attackAndDefence[i] = sheetData[ROW_ATK_DEF_ACE][i + 1];
  }

  // 通知時間を取得
  abilityDate.noticeBeforeTime = sheetData[ROW_NOTICE_BEFORE_TIME][1];

  return abilityDate;
}

/**
 * 役職別にトリガーをセット
 * @param {Object} guildAbilityDate
 */
const setTrigger = (guildAbilityDate) => {
  // 役職別関数の文字列
  const FUNC_ALL         = "execAllAbility";
  const FUNC_SUB_LEADER  = "execSubLeaderAbility";
  const FUNC_ATK_DEF_ACE = "execAtkDefAceAbility";

  // 副団長アビと攻防隊長アビの時間に応じてトリガーを作成
  for (let i = 0; i < 2; i++) {
    const timeSubLeader = guildAbilityDate.subLeader[i];
    const timeAtkDef    = guildAbilityDate.attackAndDefence[i];
    
    // 副団アビと攻防隊長アビの時間が同じ場合
    if (timeSubLeader == timeAtkDef && timeSubLeader && timeAtkDef) {
      setTriggerOfOfficer(timeSubLeader, guildAbilityDate.noticeBeforeTime, FUNC_ALL);
    } else {
      if (timeSubLeader) setTriggerOfOfficer(timeSubLeader, guildAbilityDate.noticeBeforeTime, FUNC_SUB_LEADER);
      if (timeAtkDef) setTriggerOfOfficer(timeAtkDef, guildAbilityDate.noticeBeforeTime, FUNC_ATK_DEF_ACE);
    }
  }

}

/**
 * 役職別のトリガーを作成
 * @param {number} time - アビ実行時間
 * @param {number} noticeBeforeTime - 通知時間(何分前か)
 * @param {String} strFunctionOfficer - 役職別関数の文字列
 */
const setTriggerOfOfficer = (time, noticeBeforeTime, strFunctionOfficer) => {
  let date = new Date();
  
  // アビの日時を指定
  date.setHours(time);
  date.setMinutes(0);

  // 指定時間の3分前に
  date.setMinutes(date.getMinutes() - noticeBeforeTime);

  // トリガーを作成
  ScriptApp.newTrigger(strFunctionOfficer).timeBased().at(date).create();
}

/**
 * トリガーを全て削除
 */
const removeAllTrigger = () => {
  // 全てのトリガーを取得
  const triggers = ScriptApp.getProjectTriggers();

  // トリガーを毎日確認するトリガー以外全て削除
  for (let i = 0; i < triggers.length; i++) {
    if (triggers[i].getHandlerFunction() != "chackDailyAndMakeTrigger") {
      ScriptApp.deleteTrigger(triggers[i]);
    }
  }

}
/**
 * 全てのアビを実行(Trigger Action)
 */
function execAllAbility() {
  const STR_OFFICER = STR_SUB_LEADER + "・" + STR_ATK_DEF_ACE;

  // Botメッセージを取得
  const strMsg = getBotMessage(STR_OFFICER, getNoticeBeforeTime().toString());

  // Discordに送信
  sendDiscord(strMsg);
}

/**
 * 副団アビを実行(Trigger Action)
 */
function execSubLeaderAbility() {
  // Botメッセージを取得
  const strMsg =  getBotMessage(STR_SUB_LEADER, getNoticeBeforeTime().toString());

  // Discordに送信
  sendDiscord(strMsg);
}

/**
 * 攻防隊長アビを実行(Trigger Action)
 */
function execAtkDefAceAbility() {
  // Botメッセージを取得
  const strMsg = getBotMessage(STR_ATK_DEF_ACE, getNoticeBeforeTime().toString());

  // Discordに送信
  sendDiscord(strMsg);
}

/**
 * スプレッドシートのデータを取得
 * @return {Object[][]} - スプレッドシートデータ
 */
const getSheetData = () => {
  // スプレッドシートを開く
  const sheet = SpreadsheetApp.openByUrl(SHEET_URL);
   
  // 最終列、最終行を取得
  const lastRow = sheet.getLastRow();
  const lastCol = sheet.getLastColumn();
  
  // 全ての値を配列として取得
  const sheetData = sheet.getSheetValues(1, 1, lastRow, lastCol);
  
  return sheetData;
}

/**
 * 通知時間を取得
 * @return {number} - 通知時間(何分前)
 */
const getNoticeBeforeTime = () => {
  const sheetData = getSheetData();
  return sheetData[ROW_NOTICE_BEFORE_TIME][1];
}

/**
 * Botメッセージを作成し、取得
 * @param {String} strOfficer - 役職別文字列
 * @param {String} strBeforeMinute - 通知時刻(何分前)の文字列
 * @return {String} - Botメッセージ文字列
 */
const getBotMessage = (strOfficer, strBeforeMinute) => {  
  // 役職文字列に「アビ」を追加
  strOfficer += "アビ";

  // 通知時刻文字列に「分前」を追加
  strBeforeMinute += "分前"; 

  // アビと時間を連結
  const strMsg = STR_PREFIX_BOT_MSG + strOfficer + strBeforeMinute + STR_SUFFIX_BOT_MSG;
  
  return strMsg; 
}

/**
 * Discordに送信
 * @param {String} strMsg - Botメッセージ文字列
 */
const sendDiscord = (strMsg) => {
  // Discord送信に必要な情報をセット
  const url     = DISCORD_URL;
  const payload = JSON.stringify({content: strMsg});
  const params  = {
    headers: {
      'Content-Type': 'application/json'
    },
    method: "POST",
    payload: payload,
    muteHttpExceptions: true
  };
  
  // 送信
  const response = UrlFetchApp.fetch(url, params);
  Logger.log("res : " + response.getContentText());
}
